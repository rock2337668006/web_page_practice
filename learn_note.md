JavaScript
    1. JavaScript does not have any print object or print methods.
    2. 在HTML中，要使用JS相關方法，使用<script>內容</script>


CSS與CSS3
    1. CSS：階層式樣式表，是一種用來為結構化文件（如HTML文件或XML應用）添加樣式（字型、間距和顏色等）的電腦語言。
    2. CSS3是CSS的進化版，CSS3比CSS有更多的語法來進行網頁前端的開發！
    3. CSS的出現，令HTML對於網頁前端的開發能更加地方便、簡潔！


模板使用
    以下網站有提供許多適合網頁開發且不錯看的RWD免費模板！
    1. https://blog.getbootstrap.com/2020/06/16/bootstrap-5-alpha/
    2. https://templated.co/      


Vue教學
    目前有稍微讀過「https://book.vue.tw/」這個網站的內容，個人覺得寫得不錯！


使用Docker建立Nginx伺服器
    基本上依照「https://medium.com/@xroms123/docker-%E5%BB%BA%E7%AB%8B-nginx-%E5%9F%BA%E7%A4%8E%E5%88%86%E4%BA%AB-68c0771457fb」這個網站的教學，能夠成功建立，不過在路徑的撰寫上，需要留意，要使用Linux系統能認可之路徑！




參考資料與資料引用：
1. https://www.w3schools.com/js/js_output.asp
2. https://zh.wikipedia.org/wiki/%E5%B1%82%E5%8F%A0%E6%A0%B7%E5%BC%8F%E8%A1%A8
3. https://dometi.com.tw/blog/css-css3-different/
4. 登入頁面實現：https://codertw.com/%E7%A8%8B%E5%BC%8F%E8%AA%9E%E8%A8%80/523577/
5. 搜尋頁面實現：https://blog.gtwang.org/web-development/light-javascript-table-filter-tutorial/
6. 新增、刪除功能實現：https://www.itread01.com/article/1516869132.html   




目前暫時寫到這裡，若發現此專案有錯誤，歡迎大家指教！
